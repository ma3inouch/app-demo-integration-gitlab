package net.javaguides.springboot;

public class LambdaExpressionTest {

	public static void main(String[] args) {
		// Create Greeting object with Lambda expression:
		System.out.println(Planet.MERCURY == Planet.MERCURY );
		System.out.println(Planet.MERCURY == Planet.MARS);
		System.out.println(Planet.MERCURY.equals(Planet.MERCURY));

	}

}
